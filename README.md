# coordinacio

Informació i pla de treball sobre el CFGM Forja Artística curs 2022 2023
 

## Video Conferència

[![Watch the video](https://i.imgur.com/vKb2F1B.png)](https://meet.jit.si/programacioforja)


## Organització de Centre

- [Documents d'organització de Centre](https://espai.educacio.gencat.cat/Normativa/DOIGC/Pagines/documents-pdf.asp)
- [Ensenyaments professionals d’arts plàstiques i disseny: aspectes curriculars](https://documents.espai.educacio.gencat.cat/IPCNormativa/DOIGC/CUR_Arts_Plastiques.pdf)

## CFGM Forja Artística
El CFGM de Forja Artística forma part dels estudis d'art plàstiques i dissenys regulats per la LOE


### Regulació BOE i  DOGC 

| Títol reial decret | BOE | Denominació | Total | Durada lectiva | FCT | Obra final |
| --- | --- | --- | --- | --- | --- | --- |
| 228/2015,27 de març | [Núm. 97,23.4.2015](https://www.boe.es/eli/es/rd/2015/03/27/228) | Forja Artística | 1.600h | 1.485 h | 115h | 66h |

| Títol decret Generalitat | DOGC | Denominació | Total | Durada lectiva | FCT | Obra final |
| --- | --- | --- | --- | --- | --- | --- |
| pendent | pendent | pendent | pendent | pendent | pendent | pendent |


**ATENCIÓ**
La distribució d'hores és diferent del BOE al documents [«Ensenyaments professionals d'arts plàstiques i disseny:aspectes curriculars»](https://documents.espai.educacio.gencat.cat/IPCNormativa/DOIGC/CUR_Arts_Plastiques.pdf)


| Módulos formativos | Horas currículo básico (BOE) | Hores currícum bàsic (DOGC) | EPAA* Aspectes curriculars 2021-2022 | Currículum Llotja |
| --- | --- | --- | --- | --- |
Dibujo Artístico | 80 | pendent | 
Dibujo Técnico |  50 | pendent
Volumen| 80 | pendent |
Historia del Arte y de la Forja | 50 | pendent |
Formación y orientación laboral | 50 | pendent |
Materiales y Tecnología: Forja Artística | 50 | pendent |
Taller de Forja Artística | 420 | pendent |
Obra Final | 50 | pendent |
Suma horas | 830 | pendet |


(*) Document [«Ensenyaments professionals d'arts plàstiques i disseny: aspectes curriculars»](https://documents.espai.educacio.gencat.cat/IPCNormativa/DOIGC/CUR_Arts_Plastiques.pdf)

## Documents Equip Docent

[Programacions](https://drive.google.com/drive/folders/1M0nKV7wvBMaveGzyzsKnAMrUoBzQROCz?usp=sharing)

## Bibliografia

**Tecnologia 3r Eso**
ISBN-13: 9788448149918
ISBN-10: 8448149912
Author: Joan Joseph i Gual
Published: December 2007

**Título: Materials i tecnologia : metalls : apunts teòrics per a dissenyadors industrials**
ISBN 13: 978-84-947571-3-6
Autor/es:Ferran Masip, Guillem
Editorial/es: Editorial Bombolla (2017)

**Metalografia y tratamiento térmico de los metales**
Autor/es: Yu.M.Lajtin
Editorial: Mir

**Breu història del món**
Autor: H. Gombrich, Ernst
Editorial: Empúries

**Història del Gremi de Serrallers y Ferrers de Barcelona any, 1380**
Autora:Tintó i Sala, Margarita
ISBN 13: 978-84-300-2570-1
ISBN 10: 84-300-2570-7  
Editorial/es: Gremi Serrallers de Catalunya (1980)

**Función de la arquitectura moderna**
Entrevista a Christopher Alexander
Editorial: Biblioteca Salvat

**Metalografía [Monografía]** (2005)
ISBN 13: 978-84-8301-804-0
ISBN 10: 84-8301-804-7
Autor/es:  Gil Mur, Francisco Javier ;  Manero Planella, José María
Editorial/es:  Universitat Politècnica de Catalunya. Iniciativa Digital Politècnica

**Aleaciones ligeras[Monografía]** (2001)
ISBN 13: 978-84-8301-480-6
ISBN 10: 84-8301-480-7
Autor/es:  Gil Mur, Francisco Javier ;  Manero Planella, José María ;  Rodríguez Rius, Daniel
Editorial/es:  Universitat Politècnica de Catalunya. Iniciativa Digital Politècnica

**Manual de Soldadura por Arco Elèctrico**
ISBN : 9788496960176
Publicat: Editorial Técnica

**Barandillas Rejas Cancelas de Hierro**
Dep. Legal : B41185-1968
Publicat: Editorial Blume

**Puertas Entradas Escaleras de metal**
Dep. Legal : 15051967
Publicat: Editorial Blume

**Trazado, corte y conformado**(2020)
ISBN 13: 978-84-283-4450-0
Autor/es:  Orozco Roldán, Francisco Ramón ;  López Gálvez, Cristóbal
Editorial/es:  Ediciones Paraninfo, S.A

**Mecanizado y soldadura**(2020)
ISBN 13: 978-84-283-4263-6
Autor/es:  Águeda Casado, Eduardo ... [et al.]
Editorial/es: Ediciones Paraninfo, S.A

**Título: Diccionari de l'art i dels oficis de la construcció** [Monografía](2005)
ISBN 13: 978-84-273-0743-8
ISBN 10: 84-273-0743-8
Autor/es:  Fullana, Miquel
Editorial/es:  Associacio Institucio Francesc de Borja Moll

**Diccionari d'arquitectura (1990)**
ISBN 13: 978-84-393-1476-9
ISBN 10: 84-393-1476-0
Editorial/es:  Generalitat de Catalunya

**Teoría de los objetos** (1974)
ISBN 13: 978-84-252-0786-0
ISBN 10: 84-252-0786-X
Autor/es: Moles, Abraham André (1920-1992)
Editorial/es: Editorial GG, SL

**Procesos de mecanizado** (2013)
ISBN 13: 978-84-267-2054-2     
Autor/es: Bertolin Gil, Sergio
Editorial/es: Marcombo

**Química** (2019)
Definiciones, ejemplos, gràficos...  
ISBN 13: 978-84-9974-309-7

**Fundamentos de química y física para la conservación y restauración** - (2004)  
ISBN 13: 978-84-9756-162-4
ISBN 10: 84-9756-162-7
Autor/es:San Andrés Moya, Margarita; Viña Ferrer,Sonsoles de la
Editorial/es:  Editorial Síntesis, S.A.

**Química para dummies**
ISBN: 9788432905452
Autor: John. T. Moore
Editorial: Planeta

**Curso de Química para la Formación Profesional**
ISBN: 978-84-944398-2-7
Editorial: AMV Ediciones

**Manual del Soldador**
Las Bases de la Química Moderna
Autor: Germán Hernández Riesco

**Manual de Pràcticas de Soldadura**
Autor:Alonso Marcos, Carlos
Editorial: Tècnica

**Proyectar es fácil**
Mecànica
Editorial:AFHA

**Noticias de ningua parte**
William Morris
Editorial: Taifa

**Cómo se estudia**
Autora: Serafini, Mª Teresa
Editorial: Circulo de Lectores

**Guia para la redacción y presentacion de trabajos científicos, informes técnicos y tesinas**
Autor: Comes, Prudenci
Editorial: Editorial científico-técnica la Habana

## Materials i Tecnologia

### Què acabaré sabent?
- Fonaments de física i química en relació als metalls.
- Classificar els materials des de l'àmbit de les arts aplicades al metall.
- Conèixer els materials fèrrics: la seva obtenció, propietats físiques i químiques.
- Conèixer els materials no fèrrics relacionats amb la forja artística.
- Els tractaments de protecció.


## Criteris d'avaluació
- La utilització de la terminologia específica i aplicada degudament als principis
científics i els requeriments tècnics relacionats amb els continguts del mòdul.
- Conèixer i diferenciar els metalls forjables, els aliatges principals, les
formes comercials, les propietats, els processos d'unió, conformat, de tall,
els acabats, la protecció, els processos d'obtenció i l'aplicabilitat en un
projecte de forja artística, escultòric o ornamental.
- Conèixer les mesures de seguretat i protecció per treballar amb metalls


## Continguts curriculars
1 Fonaments de física i química
2 Classificació dels materials. Propietats generals i específiques.
3 Metalls i aliatges.
4 Metalls fèrrics, els seus aliatges, propietats i les formes comercials.
5 Metalls no fèrrics relacionats amb la forja artística, propietats i formes comercials.
6 Patologies dels metalls. Sistemes de protecció

	
