## Propostes Activitats

### Escultura en metall Vanesa Muñoz i la Monica Porta

[vmunoz.com](vmunoz.com)
Vanesa Muñoz Molinero, nace en  Madrid y se traslada a Barcelona en el 2000 Cursa  Filosofía por la Universidad de Barcelona (2009), después de diplomarse como Técnico Superior Escultura en la Escola Massana de Bcn ( 2006) y Técnico Superior en Escultura de Metal en la Escuela La Palma de Madrid (1999).
Terminando los estudios de filosofía ,se alejó totalmente del enfoque antropológico, pasándo como única fuente de creación sobre aspectos matémáticos y físicos.ç
Como ella misma afirma :»….Cuanto más conozco y leo sobre física, más insignificante me resulta el ser humano y más apasionada me resultan la búsqueda del axioma ,alejado de cualquier capricho humano.
Desde las teorían de la mecánica cúantica, la teoría de la incompletitud de Gödel, la hipótesis del continuo de Cantor , o los descubrimientos sobre los agujeros negros, me resultan infinítamente más interesantes que cualquier tipo de problema existencial…».
Influenciada por el Constructivismo Ruso y las tensiones, tanto formales como cromáticas, de Anish Kapoor o Richard Serra, la abstracción de la forma y el valor emocional de la materia,pasando por la frialdad del arte óptico.
De carácter totalmente abstracto, trabaja a modo artesanal rehuyendo en cierta medida de los avances tecnológicos, pugnando por la excelencia de la técnica , como única manera de proyectar las formas que proceden de  fracciones de pensamiento, sin los cuales no podría existir.

Monica Porta
[La representació de la Barbàrie](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiZ7PODqOH4AhXFxoUKHbcfB5wQFnoECAUQAQ&url=http%3A%2F%2Fdiposit.ub.edu%2Fdspace%2Fbitstream%2F2445%2F146699%2F1%2F7.-TFM%2520M%25C3%2593NICA%2520PORTA%25202019.pdf&usg=AOvVaw2-IuhIKaXOT_dA9r9OHhYW)

## Visita del senyorJuan, treballador del metall

### Sortida: Dibuixar al carrer
Passar mitja jornada dibuixant alguna construcció o grup arquitectònic amb elements de metal·listeria. 
Prèviament havent realitzat un passeig divulgatiu per la zona. Possibilitats:
- L'Escola Industrial (Es pot aprofitar per fer una ullada a l'escola)
- Edifici Sert, Subirana i Torres Clavé (GATPAC )Sant Bernat, 10. (Pels voltants de l'escola)
- Pavellò de la República
-- visita guiada edifici: 934285457 lourdesprades@ub.edu
-- visita guiada arquitectura 669297551 info@elglobusvermell.org 

-- Poblenou: dibuixar al cementiri, i desprès anar a veure la intervenió del Jean Nouvel i l'Hangar.


## Sortida: Visita a l'ESCRBCC
- Dues sessions teòriques sobre conservació i restauració.
- Una visita a l'ESCRBCC


## Maquetació: Informació que sigui d'interés

- Treballs de maquetació sobre una graella ja constituida i amb la informació prèviament validada (ortogràfia, semàntica i ontològicament)

### Possibles tèmes

- Classificacions d'acers.
- Tipus electrodes revestits en tècnica SMAW.
